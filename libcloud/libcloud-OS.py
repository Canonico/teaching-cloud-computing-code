# This scripts work on OpenStack cloud platform provided by Chameleon
# Project (https://www.chameleoncloud.org/).
# Most of the parameters you need to set here are provided by openrc.sh
# file that you can download from Horizon OpenStack dashboard.
# If you need help, visit https://tcc.uniupo.it

# Before to run this script you must:
# 1) Download the OpenStack RC file
# 2) run 'source <openrc file>' and input your CLI password
# 3) run 'openstack token issue'
# 4) replace <token> below with the token you get from the openstack command

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

auth_username = 'your_username'
auth_password = 'your_passwors'
project_name = 'your_project_name' # It should look like CH-XXXXX or CCC-19/20

auth_url = 'https://kvm.tacc.chameleoncloud.org:5000/v3'  # It should look like https://kvm.tacc.chameleoncloud.org:5000
region_name = 'KVM@TACC' # It should looks like KVM@TACC

cls = get_driver(Provider.OPENSTACK)         

driver = cls(auth_username,auth_password,
                ex_domain_name="chameleon",
                ex_tenant_name=project_name,
                ex_force_base_url='https://kvm.tacc.chameleoncloud.org:8774/v2.1',
                ex_force_auth_url=auth_url,
                ex_force_auth_version='3.x_password',
                ex_force_auth_token='<token>')

images = driver.list_images()

i = 1
for image in images:
    print(i,"):",images.name)
    i += 1
