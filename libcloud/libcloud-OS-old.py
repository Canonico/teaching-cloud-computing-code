# This scripts work on OpenStack cloud platform provided by Chameleon
# Project (https://www.chameleoncloud.org/).
# Most of the parameters you need to set here are provided by eurcarc.sh
# file that you can download from Horizon OpenStack dashboard.

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

auth_username = '<INSERT OS_USERNAME'
auth_password = '<INSERT PASSWORD>'
project_name = '<INSERT PROJECT NAME'> # It looks like CH-XXXXX

auth_url = '<INSERT OS_AUT_URL>'  # It looks like https://kvm.tacc.chameleoncloud.org:5000
region_name = '<INSERT OS_REGION_NAME' # It looks like KVM@TACC

provider = get_driver(Provider.OPENSTACK)
conn = provider(auth_username,auth_password,ex_force_auth_url=auth_url,ex_force_auth_version='3.x_password',ex_tenant_name=project_name,ex_force_service_region=region_name,api_version='2.1')

image_id = '<INSERT THE IMAGE ID>' # For example 'b89f851a-d32a-410c-bf75-9deb2b1c2b63' corresponds to CENTOS 7
image = conn.get_image(image_id)                     

flavor_id = '<INSERT FLAVOR ID>' # For example '3'corresponds to m1.medium
flavor = conn.ex_get_size(flavor_id)

instance = conn.create_node(name='PracticeInstance', image=image, size=flavor)  # Launch the instance. Please note that this
                                                                                # is a very basic way to launch an image
                                                                                # without providing key pairs ID or security
                                                                                # group ID.

