from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

ComputeEngine = get_driver(Provider.GCE)

# The ComputeEngine function should look like this:
#driver = ComputeEngine('libcloud@cloud-computing-class-disit.iam.gserviceaccount.com', '/home/mex/work/teaching/hpc/code/cloud-computing-class-disit-b82e0457d6c8.json',
#                       project='cloud-computing-class-disit',
#                       datacenter='us-central1-a')

driver = ComputeEngine('your_service_account_email', 'path_to_pem_file',
                       project='your_project_id',
                       datacenter='availability_zone')

images = driver.list_images()

i = 1
for image in images:
    print(i,"):",images.name)
    i += 1
