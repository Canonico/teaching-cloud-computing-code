from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

cls = get_driver(Provider.EC2)
#driver = cls('', '', region='us-east-1')
driver = cls('access key', 'secret key', region='availability zone')

images = driver.list_images()

#print(images)

i = 1
for image in images:
    if image.name is not None: #AWS prints "None" as name for some items
        print(i,"):",image.name)
        i += 1

