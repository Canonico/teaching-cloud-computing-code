#!/bin/bash
#This script monitors the cpu usage and if it is below a threshold, the VM will shutted down.

# CPU usage (5% for now)
cpu_threshold=5

# check CPU usage
check_interval=60

# Time period of inactivity (1 hour)
inactivity_period=$((60 * 60))

inactivity_timer=0

folder_name="$HOME/cpu_logs"

while true; do
    # Get the CPU usage percentage
    cpu_usage=$(top -b -n1 | grep "Cpu(s)" | awk '{print $2 + $4}')

    # If the CPU usage is less than the threshold
    if (( $(echo "$cpu_usage < $cpu_threshold" | bc -l) )); then
        # Increment the inactivity timer
        inactivity_timer=$((inactivity_timer + check_interval))

        # Check if the inactivity period has elapsed
        if (( $inactivity_timer >= $inactivity_period )); then
            # Shut down the OS
            if [ ! -d "$folder_name" ]; then
                mkdir "$folder_name"
            fi
            echo "$(date +%T): CPU usage has been below $cpu_threshold% for $inactivity_period seconds. Shutting down..." >> $folder_name/$(date +%Y-%m-%d)_log.txt
            sudo shutdown now
            break
        fi
    else
        # Reset the inactivity timer if CPU usage is above threshold
        inactivity_timer=0
    fi

    # Wait for the next check interval
    sleep $check_interval
done
