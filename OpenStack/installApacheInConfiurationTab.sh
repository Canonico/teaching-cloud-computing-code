#!/bin/bash

#Script author: Elviane Lucrece
#More info: tcc.uniupo.it

sudo su
apt-get update > update-out.txt 2> update-err.txt
apt-get upgrade > upgrade-out.txt 2> upgrade-err.txt
apt-get install -y apache2 > apacheInstall-out.txt 2> apacheInstall-err.txt
apt-get install -y php libapache2-mod-php > installPHP-out.txt 2> installPHP-err.txt
echo "Installazione di Apache e PHP via bash" > /var/www/html/index.html
service apache2 restart > apacheStatus-out.txt 2> apacheStatus-err.txt
