#!/bin/bash
apt update
apt install -y wget ca-certificates gnupg
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" | tee /etc/apt/sources.list.d/pgdg.list
apt update
apt install -y postgresql-13 postgresql-client-13

#configure postgresql
su - postgres <<EOF
psql -c "ALTER USER postgres WITH PASSWORD 'mypassword';"
EOF

#create database and user
su - postgres <<EOF
psql -c "CREATE DATABASE mydatabase;"
psql -c "CREATE USER myuser WITH PASSWORD 'mypassword';"
psql -c "GRANT ALL PRIVILEGES ON DATABASE mydatabase TO myuser;"
EOF

#configure pg_hba.conf to allow connections from all hosts
echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/13/main/pg_hba.conf
echo "listen_addresses = '*'" >> /etc/postgresql/13/main/postgresql.conf

#restart postgresql to apply changes
systemctl restart postgresql.service

#test connection
PGPASSWORD=mypassword psql -h localhost -U myuser mydatabase -c "SELECT version();"

echo "PostgreSQL is configured "
