#Author: Enrico Bisio
#Apache and PHP installation
#!/bin/bash
apt-get update > update-out.txt 2> update-err.txt
apt-get upgrade > upgrade-out.txt 2> upgrade-err.txt
apt-get install -y apache2 > apacheInstall-out.txt 2> apacheInstall-err.txt
apt-get install -y php libapache2-mod-php > installPHP-out.txt 2> installPHP-err.txt
echo "Apache and PHP installed" > /var/www/html/index.html
service apache2 restart > apacheStatus-out.txt 2> apacheStatus-err.txt


