#!/bin/bash

sudo apt-get update

sudo apt-get install -y apache2 >> /tmp/log_apache2.txt

sudo apt-get install -y php libapache2-mod-php >> /tmp/log_php.txt

sudo service apache2 restart

sudo apt-get install -y mariadb-server

sudo service apache2 restart

sudo systemctl start mariadb.service

sudo sh -c 'echo "Web server and Maria DB up and runnnig" >> /var/www/html/index.html 2> /tmp/error.txt'

sudo sh -c 'echo "<?php" >> /var/www/html/info.php' #an alternative to 'sh -c' is to change the permissions on the folder and file

sudo sh -c 'echo "phpinfo();" >> /var/www/html/info.php'

sudo sh -c 'echo "?>" >> /var/www/html/info.php'

sudo apt-cache search php

sudo apt-get install php-mysql php-curl php-gd php-intl php-pear php-imagick php-imap php-memcache php-pspell php-snmp php-sqlite3 php-tidy php-xmlrpc php7.4-xml #removed php-mycrypt and php-recode, which were deprecated packages, and updated php7.0-xml to php7.4-xml

sudo service apache2 restart
