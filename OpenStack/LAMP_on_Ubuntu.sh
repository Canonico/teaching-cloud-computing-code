#Author: Rosilde Garavoglia
#Script tested on Ubuntu 20.04
#
#LAMP installation on Ubuntu
#!/bin/bash
sudo apt update > /tmp/update-out.txt
sudo apt install -y apache2 > /tmp/apacheInstall-out.txt
sudo apt install -y  php php-common libapache2-mod-php php-cli php-fpm php-mysql php-json php-opcache php-gmp php-curl php-intl php-mbstring php-xmlrpc php-gd php-xml php-zip > /tmp/installPHP-out.txt
sudo apt-get install -y mariadb-server > /tmp/mariadb.txt
sudo mysql -u root -e "SET password for 'root'@localhost= PASSWORD('root');"
sudo mysql -u root -p"root" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
sudo mysql -u root -p"root" -e "DELETE FROM mysql.user WHERE User=''"
sudo mysql -u root -p"root" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
sudo mysql -u root -p"root" -e "FLUSH PRIVILEGES"
sudo mysql -u root -p'root' -e "CREATE DATABASE joomla_db;"
sudo mysql -u root -p'root' -e "CREATE USER 'user1'@'localhost' IDENTIFIED by 'password';"
sudo mysql -u root -p'root' -e "GRANT ALL PRIVILEGES ON joomla_db.* TO 'user1'@'localhost';"
sudo mysql -u root -p'root' -e "FLUSH PRIVILEGES;"
sudo service apache2 restart > /tmp/apacheStatus-out.txt
