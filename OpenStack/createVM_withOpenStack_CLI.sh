#!/bin/bash

# Uno script che permette all'utente di avviare una VM usando
# OpenStack CLI
#
# Authors FIRAS HSOUMI and YASSINE HMID.
# July 2024

# RC_file
read -p "Enter the path to your OpenStack RC file: " RC_FILE

modify_rc_file() {
    	local rc_file=$1
    	local modified_rc_file="/tmp/modified_rc_file.sh"

    	# Rimuovere le righe che chiedono la password
    	grep -v '($OS_USERNAME) Please enter your Chameleon CLI password:' "$rc_file" | \
    	grep -v 'read -sr OS_PASSWORD_INPUT' | \
    	grep -v 'export OS_PASSWORD=$OS_PASSWORD_INPUT' > "$modified_rc_file"

    	cat >> "$modified_rc_file" <<EOF
    	while true; do
    		echo "($OS_USERNAME) Enter your Chameleon CLI password: "
    		read -sr OS_PASSWORD_INPUT
    		if openstack project list --os-password "\$OS_PASSWORD_INPUT" 2>/dev/null; then
    			export OS_PASSWORD=\$OS_PASSWORD_INPUT
			break
    		else
    			echo "Incorrect password, please try again."
     		fi
     	done
EOF

	echo "$modified_rc_file"
}

MODIFIED_RC_FILE=$(modify_rc_file "$RC_FILE")

# Source RC_file
if [ -f "$MODIFIED_RC_FILE" ]; then
	source "$MODIFIED_RC_FILE"
else
	echo "RC file not found. Exiting..."
	exit 1
fi

# Funzione per selezionare un'opzione
select_option() {
	select opt in "$@"; do
		if [ -n "$opt" ]; then 
			echo "$opt"
			return
		else
			echo "Invalid selection!"
		fi
	done
}


# Nome VM
read -p "Enter the name of the VM: " VM_NAME

# List immagini disponibili e ne seleziona una
images=$(openstack image list --format value --column Name)
images_arr=($images)
echo "Select an image:"
image=$(select_option "${images_arr[@]}")

# List  le chiave disponibili
keys=$(openstack keypair list --format value --column Name)
keys_arr=($keys)
echo "Select a key:"
key=$(select_option "${keys_arr[@]}")

# List i flavors disponibili nella piattaforma Openstack
flavors=$(openstack flavor list --format value --column Name)
flavors_arr=($flavors)
echo "Select a flavor:"
flavor=$(select_option "${flavors_arr[@]}")

# List gli ips disponibili (Solo quelli che non sono in uso)
ips=$(openstack floating ip list --format value --column "Floating IP Address" --status DOWN)
ips_arr=($ips)
# Se non ce ne sono  ips  disponibili, Ne creamo  uno
if [ ${#ips_arr[@]} -eq 0 ]; then
	echo "No available public IPs. Creating a new public IP..."
	ALLOCATED_IP=$(openstack floating ip create public --format value --column floating_ip_address)
	ips_arr=($ALLOCATED_IP)
fi

echo "Select a public IP:"
ip=$(select_option "${ips_arr[@]}")

# Creazione della VM
echo "Creating the VM..."
openstack server create --image "$image" --flavor "$flavor" --key-name "$key" "$VM_NAME"

# Assegnazione dell'ip pubblico
echo "Assigning the public IP..."
openstack server add floating ip "$VM_NAME" "$ip"

#controllo degli errori
if [  $? -eq 0 ]; then
	echo "The VM has been successfully created and the public IP has been assigned!"
else
	echo "Error creating the VM or assigning the IP!"
fi
