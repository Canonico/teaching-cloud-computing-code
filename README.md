In these directories, we put all the source code used during the
Cloud Computing course at the University of Piemonte Orientale (Italy).

This educational material is part of the other material that you can find
in the web site called "Teaching Cloud Computing": http://tcc.uniupo.it.

The scripts proposed are related to 
- Apache libcloud, Boto, the multi-cloud libraries since we are 
interested in avoiding the lock-in problem.
- chi: a library for OpenStack provided by Chameleon project since we are interested on open source projects

To use the libcloud scripts, you have to install the library by run the following command:
$ pip3 install --user apache-libcloud

To use the boto scripts, you have to install the library by run the following command:
$ pip3 install --user boto3

To use the chi scripts, you have to install the library by run the following command:
$ pip3 install --user python-chi

For support or comment: tcc@uniupo.it
