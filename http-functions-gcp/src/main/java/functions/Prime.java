package functions;

import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedWriter;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

/**
 * POST request to determinate if a {@code number} is prime.
 *
 * @author Lorenzo Ferron
 * @version 20210817
 */
public class Prime implements HttpFunction {
    private static final Logger LOGGER = Logger.getLogger(Prime.class.getName());

    // Use GSON (https://github.com/google/gson) to parse JSON content.
    private static final Gson GSON = new Gson();

    @Override
    public void service(HttpRequest request, HttpResponse response) throws com.google.gson.JsonSyntaxException, com.google.gson.JsonIOException, java.io.IOException {
        // check if request is a POST
        if (!"POST".equals(request.getMethod())) {
            response.setStatusCode(HttpURLConnection.HTTP_BAD_METHOD);
            return;
        }

        long number = 0;

        // Default values avoid null issues and exceptions from get() (optionals)
        String contentType = request.getContentType().orElse("");

        if (!"application/json".equals(contentType)) {
            // Invalid or missing "Content-Type" header
            response.setStatusCode(HttpURLConnection.HTTP_UNSUPPORTED_TYPE);
            return;
        }

        // '{"number":"500"}'
        JsonObject body = GSON.fromJson(request.getReader(), JsonObject.class);
        // Exception if number in request is an empty string.
        if (body.has("number")) number = body.get("number").getAsLong();

        // Verify that a number was provided and valid
        if (number <= 0) {
            response.setStatusCode(HttpURLConnection.HTTP_BAD_REQUEST);
            return;
        }

        response.setStatusCode(HttpURLConnection.HTTP_OK);
        response.setContentType("application/json; charset=utf-8");

        LOGGER.info("Processed number: " + number);

        // prepare response
        JsonObject bodyResponse = new JsonObject();
        bodyResponse.addProperty("response", isPrime(number));

        // Handle the main request.
        BufferedWriter writer = response.getWriter();
        writer.write(bodyResponse.toString());
    }

    /**
     * Checks if {@code number} is prime.
     *
     * @param number to check
     * @return {@code true} if {@code number} is prime, {@code false} otherwise
     */
    private Boolean isPrime(long number) {
        if ((number & 1) == 0) return false;
        if (number <= 2) return true;
        for (int i = 3; i < Math.floor(Math.sqrt(number)); i += 2) if (number % i == 0) return false;
        return true;
    }
}
